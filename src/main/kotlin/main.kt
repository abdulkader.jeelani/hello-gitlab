package gitlab.cd

import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.server.Netty
import org.http4k.server.asServer


open class Application {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runServer()
        }

        private fun runServer() {
            val app = { request: Request ->
                Response(OK).body(
                    greet("${request.query("name")}!")
                )
            }
            val server = app.asServer(Netty(9000)).start()
        }

        public fun greet(name: String) = "Welcome $name"
    }
}
