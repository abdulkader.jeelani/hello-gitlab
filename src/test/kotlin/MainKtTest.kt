import gitlab.cd.Application
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class MainKtTest {

    @Test
    fun greet() {
        val message = Application.greet("you")
        assertEquals("Welcome you", message)
    }
}